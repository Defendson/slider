# slider with react

###How to open DEMO:

```
npm i
npm run build
npm start
open http://127.0.0.1:5757
```

### Include Params
```
/* Default Params */
canDrag: boolean = false, // включить возможность перетаскивания слайдов
canScroll: boolean = false, // включает возможность листания слайдов на скролл
hasArrows: boolean = true, // отображение либо скрытие стрелок
isAdaptive: boolean = false, // включает адаптивную ширину
isReverse: boolean = false, // включает обратный порядок слайдов
setShownSlides: boolean = 4, // количество отображаемых слайдов (если нет адаптивности)
setAnimationSpeed: boolean = 8 // скорость анимации слайдов - принимает значение от 1 до 10
```
