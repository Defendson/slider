const webpack = require('webpack');
const path = require('path');

module.exports = {
	devtool: 'eval-source-map',
	entry: './src/js',
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				use: ['babel-loader']
			}
		]
	},
	resolve: {
		extensions: ['*', '.jsx', '.js']
	},
	output: {
		filename: 'bundle.min.js',
		path: path.resolve(__dirname, 'static')
	},
	externals: {
		'react': 'React',
		'react-dom': 'ReactDOM'
	}
};
