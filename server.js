const express = require('express');
const app = express();
const path = require('path');

app.use('/static', express.static(path.resolve(__dirname, 'static')));
app.use('/static/react', express.static(path.resolve(__dirname, 'node_modules', 'react', 'umd')));
app.use('/static/react-dom', express.static(path.resolve(__dirname, 'node_modules', 'react-dom', 'umd')));
app.get('/', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'index.html'))
});


app.listen('5757', () => {
	console.log('Server was started on: http://127.0.0.1:5757');
});
