import React from 'react';
import Slider from './oslider';

class Carousel extends React.PureComponent {
	constructor(props) {
		super(props);

		const slideList = [];

		for(let i = 1; i <= 3; i++) {
			slideList.push(<div key={i} className='carousel__item'>slide {i}</div>)
		}

		this.slides = this.getData();

		this.children = [];
		this.state = {
			children: [],
			currentSlideListNumber: 0
		};
	}

	componentWillMount() {
		this.getSlideList = this.getSlideList.bind(this);
	}

	getSlideList() {
		const listNumber = this.state.currentSlideListNumber;

		this.setState({
			currentSlideListNumber: listNumber + 1
		});

		const newSlides = this.slides[listNumber];

		console.log(newSlides);

		setTimeout(() => {
			if (newSlides) {
				this.setState({
					children: this.state.children.concat(newSlides)
				});
			}
		}, 1000);
	}

	getData() {
		const slides = [];
		const slideList1 = [];
		const slideList2 = [];
		const slideList3 = [];

		let i = 1;

		while(i <= 10) {
			slideList1.push(<div key={i} className='carousel__item'>slide {i}</div>);
			i++;
		}

		while(i <= 20) {
			slideList2.push(<div key={i} className='carousel__item'>slide {i}</div>);
			i++;
		}

		while(i <= 30) {
			slideList3.push(<div key={i} className='carousel__item'>slide {i}</div>);
			i++;
		}

		slides.push(slideList1);
		slides.push(slideList2);
		slides.push(slideList3);

		return slides;
	}

	render() {
		return (
			<div className='carousel'>
				<h1>Lazy load</h1>

				<Slider
					isAdaptive={true}
					setShownSlides={4}
					canDrag={true}
					canScroll={true}
					onReadyToLoad={this.getSlideList}
					setIndent={5}
				>
					{this.state.children}
				</Slider>
			</div>
		);
	}
}

export default Carousel;
