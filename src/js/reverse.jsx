import React from 'react';
import Slider from './oslider';

class Carousel extends React.PureComponent {
	constructor(props) {
		super(props);

		const slideList = [];

		for(let i = 1; i <= 10; i++) {
			slideList.push(<div key={i} className='carousel__item'>slide {i}</div>)
		}

		this.state = {
			children: slideList
		}
	}

	render() {
		return (
			<div className='carousel'>
				<h1>Reverse</h1>

				<Slider
					isReverse={true}
				>
					{this.state.children}
				</Slider>
			</div>
		);
	}
}

export default Carousel;
