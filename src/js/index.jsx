import React from 'react';
import ReactDOM from 'react-dom';
import Default from './default';
import Adaptive from './adaptive';
import Scrollable from './scrollable';
import LazyLoad from './lazyLoad';
import Reverse from './reverse';
import Drag from './drag';

ReactDOM.render(
	<div>
		{/*<Default/>*/}
		{/*<br/><br/><br/><br/>*/}

		{/*<Adaptive/>*/}
		{/*<br/><br/><br/><br/>*/}

		{/*<Drag/>*/}
		{/*<br/><br/><br/><br/>*/}

		{/*<Scrollable/>*/}
		{/*<br/><br/><br/><br/>*/}

		{/*<LazyLoad/>*/}
		{/*<br/><br/><br/><br/>*/}

		{/*<Reverse/>*/}
		{/*<br/><br/><br/><br/>*/}

		<LazyLoad />
	</div>,
	document.getElementById('root')
);
