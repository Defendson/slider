import React from 'react';

export default class Carousel extends React.Component {
	constructor(props) {
		super(props);

		this.defaultSettings = {
			canDrag: false,
			canScroll: false,
			hasArrows: true,
			isAdaptive: false,
			isReverse: false,
			setShownSlides: 4,
			setAnimationSpeed: 8,
			setIndents: 10,
			onReadyToLoad: () => {}
		};

		this.settings = Object.assign(this.defaultSettings, props);
		delete (this.settings.children);

		this.slideWidth = 0;
		this.sliderWidthPercent = 100;
		this.indentWidth = this.settings.setIndents;
		this.indentWidthPercent = 0;
		this.sliderWidth = 0;
		this.slideWidthPercent = 100;
		this.stepPrevX = 0;
		this.stepNextX = 0;
		this.positionPrev = 0;
		this.minPositionDiff = 0;
		this.maxPositionDiff = 0;
		this.slidesShownCount = 0;

		this.state = {
			children: props.children,
			childrenCount: Array.isArray(props.children) && props.children.length ? props.children.length : 0,
			isSmooth: true,
			isTouched: false,
			isWheeling: false,
			isRendered: false,
			slideWidthPercent: 0,
			positionX: 0,
			isReadyToLoad: typeof this.props.onReadyToLoad === 'function'
		};
	}

	componentWillMount() {
		this.handleTouchStart = this.handleTouchStart.bind(this);
		this.handleTouchMove = this.handleTouchMove.bind(this);
		this.handleTouchEnd = this.handleTouchEnd.bind(this);

		this.handleMouseDown = this.handleMouseDown.bind(this);
		this.handleMouseMove = this.handleMouseMove.bind(this);
		this.handleMouseUp = this.handleMouseUp.bind(this);
		this.handleWheel = this.handleWheel.bind(this);

		this.handleMouseLeave = this.handleMouseLeave.bind(this);

		this.wheelSlide = this.wheelSlide.bind(this);

		this.handleArrowNext = this.handleArrowNext.bind(this);
		this.handleArrowPrev = this.handleArrowPrev.bind(this);
		this.updateDimensions = this.updateDimensions.bind(this);
		this.getCustomizedSlides = this.getCustomizedSlides.bind(this);
		this.getNewPositionX = this.getNewPositionX.bind(this);
		this.getArrows = this.getArrows.bind(this);

		this.getScrollHandlers = this.getScrollHandlers.bind(this);
		this.getDragHandlers = this.getDragHandlers.bind(this);
		this.getWrapperStyle = this.getWrapperStyle.bind(this);

		this.scrollHandlers = this.getScrollHandlers();
		this.dragHandlers = this.getDragHandlers();
	}

	componentDidMount() {
		this.state.isReadyToLoad && this.props.onReadyToLoad();
		this.updateDimensions();
		window.addEventListener("resize", this.updateDimensions);
	}

	componentWillReceiveProps(nextProps) {
		const nextChildrenLength = nextProps.children.length;

		if (nextChildrenLength && nextChildrenLength !== this.state.childrenCount) {
			this.setState({
				children: nextProps.children,
				childrenCount: nextProps.children.length
			}, () => {
				this.setState({
					isReadyToLoad: true
				});
				nextProps.children.length && this.updateDimensions();
			});
		}
	}

	componentWillUnmount() {
		window.removeEventListener("resize", this.updateDimensions);
	}

	/**
	 * Update dimensions
	 */
	updateDimensions() {
		this.sliderWidth = this.refs.sliderContainer.clientWidth;

		if (this.state.childrenCount) {
			this.slideWidth = this.refs.slide.clientWidth;

			if (this.settings.isAdaptive) {
				this.indentWidthPercent = 100 / this.sliderWidth * this.indentWidth;
				this.slidesShownCount = this.sliderWidth / (this.slideWidth + this.indentWidth);
				this.slideWidthPercent = this.sliderWidthPercent / this.slidesShownCount;

			} else {
				this.slidesShownCount = this.settings.setShownSlides || this.defaultSettings.setShownSlides;
				this.slideWidthPercent = this.sliderWidthPercent / this.slidesShownCount;
			}
			this.sliderWrapperWidthPercent = this.state.childrenCount * this.slideWidthPercent - this.indentWidthPercent;
			this.maxPositionDiff = -(this.sliderWrapperWidthPercent - this.sliderWidthPercent) >= 0
				? 0
				: -(this.sliderWrapperWidthPercent - this.sliderWidthPercent);

			this.setState({
				slideWidthPercent: this.slideWidthPercent
			});
		} else {
			this.sliderWrapperWidthPercent = this.sliderWidthPercent;
		}

		this.throwSlide();
	}

	catchSlide(currentPosX) {
		this.stepPrevX = currentPosX !== 0 ? currentPosX / this.sliderWidth * 100 : 0;
		this.positionPrev = this.state.positionX;

		this.setState({
			isSmooth: false
		});
	}

	wheelSlide(scrollSize) {
		const newPositionX = this.state.positionX - scrollSize;
		const positionX = this.getCheckedPositionX(newPositionX);

		this.stepPrevX = this.stepNextX;

		this.setState({
			positionX: positionX
		});
	}

	moveSlide(currentPosX) {
		this.stepNextX = currentPosX !== 0
			? currentPosX / this.sliderWidth * 100
			: 0;

		const diff = this.stepPrevX - this.stepNextX;
		const positionX = this.getCheckedPositionX(this.state.positionX - diff);

		this.stepPrevX = this.stepNextX;

		this.setState({
			positionX: positionX
		});
	}

	throwSlide() {
		const positionX = this.getCheckedPositionX(this.getNewPositionX());

		this.positionPrev = positionX;

		this.setState({
			isSmooth: true,
			positionX: positionX,
			isTouched: false,
			isWheeling: false
		});
	}

	handleMouseDown(e) {
		this.setState({
			isTouched: true
		});

		const currentPosX = e.clientX;
		this.catchSlide(currentPosX);
	}

	handleMouseMove(e) {
		if (this.state.isTouched) {
			const currentPosX = e.clientX;
			this.moveSlide(currentPosX);
		}
	}

	handleMouseUp() {
		this.throwSlide();
	}

	handleWheel(e) {
		e.preventDefault();
		const MIN_SCROLL_WIDTH = 2;
		const scrollSize = Math.abs(e.deltaY) >= Math.abs(e.deltaX) ? e.deltaY : e.deltaX;

		if (this.state.isWheeling) {
			if (Math.abs(scrollSize) > MIN_SCROLL_WIDTH) {
				this.wheelSlide(scrollSize);
			} else {
				this.setState({
					isWheeling: false
				});
				this.throwSlide();
			}
		} else {
			this.setState({
				isWheeling: true
			});
		}
	}

	handleMouseLeave() {
		if (this.state.isTouched || this.state.isWheeling) {
			this.throwSlide();
		}
	}

	/**
	 * Handle touch start
	 * @param e
	 */
	handleTouchStart(e) {
		const currentPosX = e.changedTouches[0].clientX;
		this.catchSlide(currentPosX);
	}

	/**
	 * Handle touch move
	 * @param e
	 */
	handleTouchMove(e) {
		if (this.state.positionX <= this.minPositionDiff && this.state.positionX == this.maxPositionDiff) {
			return false;
		}

		const currentPosX = e.changedTouches[0].clientX;

		this.moveSlide(currentPosX);
	}

	/**
	 * Handle touch end
	 */
	handleTouchEnd() {
		this.throwSlide();
	}

	/**
	 * Get new position X
	 * @returns {*}
	 */
	getNewPositionX() {
		const prevPosition = this.positionPrev;
		const currentPosition = this.state.positionX;
		const diffDistance = currentPosition - prevPosition;

		if (!diffDistance) {
			return prevPosition;
		}

		const oneStep = this.slideWidthPercent;
		const wholeDiffCount = Math.floor(diffDistance / oneStep);
		const wholeDiffDistance = diffDistance < 0
			? wholeDiffCount * oneStep
			: wholeDiffCount * oneStep + oneStep;

		const newPositionX = prevPosition + wholeDiffDistance;

		return newPositionX;
	}

	/**
	 * Get checked position x
	 * @param positionX
	 * @returns {*}
	 */
	getCheckedPositionX(positionX) {
		if (this.slidesShownCount && this.slideWidthPercent && this.maxPositionDiff) {
			const coefficient = Math.abs(this.slidesShownCount * this.slideWidthPercent * 0.5);

			if (positionX <= this.maxPositionDiff + coefficient
				&& typeof this.props.onReadyToLoad === 'function'
				&& this.state.isReadyToLoad) {
				this.setState({
					isReadyToLoad: false
				}, this.props.onReadyToLoad);
			}

			if (positionX >= this.minPositionDiff) {
				positionX = this.minPositionDiff;
			} else if (positionX <= this.maxPositionDiff) {
				positionX = this.maxPositionDiff;
			}

			return positionX;
		} else {
			return 0;
		}
	}

	/**
	 * Get item width
	 * @returns {*}
	 */
	getSlideWidth() {
		return this.settings.isAdaptive
			? 'auto'
			: this.state.slideWidthPercent + '%';
	}

	getSlideIndents() {
		return this.settings.isAdaptive
			? this.indentWidth + 'px'
			: '0'
	}

	/**
	 * Get customized slider
	 * @returns {*}
	 */
	getCustomizedSlides() {
		if (this.state.childrenCount) {
			const slides = this.state.children;

			const customizedSlider = slides.map((slide, index) => (
				<div
					key={index}
					className="oslider__slide"
					style={{
						width: this.getSlideWidth(),
						marginRight: this.getSlideIndents()
					}}
				>
					<div className="oslider__slide-wrapper">
						<div className="oslider__slide-inner" ref="slide">
							{slide}
						</div>
					</div>
				</div>
			));

			return this.settings.isReverse === true
				? customizedSlider.reverse()
				: customizedSlider;
		}
	}

	/**
	 * Get arrows
	 * @returns {*}
	 */
	getArrows() {
		if (!this.settings.hasArrows) {
			return false;
		}

		return (
			<div>
                <span onMouseUp={this.handleArrowPrev}
					  className={`oslider__arrow oslider__arrow_prev ${this.state.positionX >= this.minPositionDiff ? 'oslider__arrow_disabled' : ''} ${this.minPositionDiff == this.maxPositionDiff ? 'oslider__arrow_hidden' : ''}`} />
				<span onMouseUp={this.handleArrowNext}
					  className={`oslider__arrow oslider__arrow_next ${this.state.positionX <= this.maxPositionDiff ? 'oslider__arrow_disabled' : ''} ${this.minPositionDiff == this.maxPositionDiff ? 'oslider__arrow_hidden' : ''}`} />
			</div>
		)
	}

	/**
	 * Handle previos arrow
	 */
	handleArrowPrev() {
		let positionX = this.getCheckedPositionX(this.state.positionX + this.slideWidthPercent);

		if (this.state.positionX !== positionX) {
			this.setState({
				positionX: positionX
			});
		}
	}

	/**
	 * Handle next arrow
	 */
	handleArrowNext() {
		let positionX = this.getCheckedPositionX(this.state.positionX - this.slideWidthPercent);

		if (this.state.positionX !== positionX) {
			this.setState({
				positionX: positionX
			});
		}
	}

	/**
	 * Get wrapper style
	 * @returns Object
	 */
	getWrapperStyle() {
		const positionX = this.state.positionX !== 0
			? `${this.state.positionX}%`
			: `0px`;

		const style = {
			transform: `translateX(${positionX})`
		};

		const animationSpeed = this.settings.setAnimationSpeed;
		const speed = animationSpeed <= 10 && animationSpeed >= 1
			? (10 - animationSpeed + 1) * 100
			: 0;

		if (this.state.isSmooth) {
			style.transition = `transform ${speed}ms ease-out`;
		} else {
			delete style.transition;
		}

		return style;
	}

	getDragHandlers() {
		return this.settings.canDrag && {
			onTouchStart: this.handleTouchStart,
			onTouchMove: this.handleTouchMove,
			onTouchEnd: this.handleTouchEnd,
			onMouseDown: this.handleMouseDown,
			onMouseMove: this.handleMouseMove,
			onMouseUp: this.handleMouseUp,
			onMouseLeave: this.handleMouseLeave
		}
	}

	getScrollHandlers() {
		return this.settings.canScroll && {
			onWheel: this.handleWheel
		}
	}

	render() {
		return (
			<div>
				<div
					className="oslider"
					{...this.scrollHandlers}
				>
					<div className="oslider__container" ref="sliderContainer">
						<div
							className="oslider__wrapper"
							{...this.dragHandlers}
							style={this.getWrapperStyle()}
						>
							{this.getCustomizedSlides()}
						</div>
					</div>

					{this.getArrows()}
				</div>
			</div>
		);
	}
}
